//Header

#ifndef DOT_H
#define DOT_H

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <string>
using namespace std;

//The dot that will move around on the screen
class Dot
{
public:
	//The dimensions of the dot
	static const int DOT_WIDTH = 20;
	static const int DOT_HEIGHT = 20;

	//Maximum axis velocity of the dot
	static const int DOT_VEL = 400;

	//Initializes the variables
	Dot(float x, float y);

	//Takes key presses and adjusts the dot's velocity
	void handleEvent(SDL_Event& e);

	//Moves the dot
	void move(float timeStep, SDL_Rect& wall);


	//Shows the dot on the screen
	void render();

private:
	float mPosX, mPosY;
	float mVelX, mVelY;
	//Dot's collision box
	SDL_Rect mCollider;
};

#endif
#include "Wall.h"
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <string>

//The window renderer
//SDL_Renderer* gRenderer = NULL;

Wall::Wall()
{
	
}

Wall::Wall(int x, int y, int w, int h,int pid,bool Coll_check,bool moving_up,int OriX,int OriY)
{
	//Initialize the position
	mPosX = x;
	mPosY = y;
	mPosW = w;
	mPosH = h;
	pic_id = pid;
	CC = Coll_check;

	OX = OriX;
	OY = OriY;

	//Set collision box dimension
	mCollider.x = mPosX;
	mCollider.y = mPosY;
	mCollider.w = mPosW;
	mCollider.h = mPosH;

}

Wall::~Wall()
{

}


/*Wall::render(vector<Wall>& Walls)
{
	//Render wall
	SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xFF);
	for (unsigned int i = 0; i < Walls.size(); i++){

		SDL_RenderDrawRect(gRenderer, Walls[i]);
	}
}
*/
SDL_Rect Wall::getRect()
{
	if (CC)
		return mCollider;

	
}

SDL_Rect Wall::getRect2()
{
	return mCollider;
}

int Wall::getPicID()
{
	return pic_id;
}

int Wall::Get_X()
{
	return mCollider.x;
}

int Wall::Get_Y()
{
	return mCollider.y;
}

int Wall::Get_OX()
{
	return OX;
}

int Wall::Get_OY()
{
	return OY;
}

int Wall::Set_Rectx(int x)
{
	mCollider.x = x;
	
	return mCollider.x;
}

int Wall::Set_Recty( int y)
{
	
	mCollider.y = y;
	return mCollider.y;
}